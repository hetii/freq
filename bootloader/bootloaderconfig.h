#ifndef __bootloaderconfig_h_included__
#define __bootloaderconfig_h_included__

#define USB_CFG_IOPORTNAME      D
#define USB_CFG_DMINUS_BIT      7
#define USB_CFG_DPLUS_BIT       2

#define HAVE_EEPROM_PAGED_ACCESS    1
#define HAVE_EEPROM_BYTE_ACCESS     1
#define HAVE_CHIP_ERASE             0

#endif /* __bootloader_h_included__ */
