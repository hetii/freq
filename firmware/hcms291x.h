#ifndef hcms291x_H
#define hcms291x_H

#include <stdint.h>
// Define minimum 4 pins for the LED display:
#define HCMS_DATA_IN B,3 // The display's (DIN) data in pin.
#define HCMS_RS      B,1 // The display's (RS)  register select pin.
#define HCMS_CLOCK   B,5 // The display's (CLK) clock pin.
#define HCMS_CE      B,2 // The display's (CE)  chip enable pin.
#define HCMS_RESET   B,0 // The display's (RST) reset pin. Connect to MCU or pullup by 4k7.

#define HCMS_DISPLAY_LENGTH 8 // number of bytes needed to pad the string
#define HCMS_USE_HARDWARE_SPI 1

/*OOR ++GG C  D
  USS 55NNCL RI
  TCT VVDDEK SN
________________
| ... ...... .. |
|# # # # # # # #|
|   .   .  .    |
 \______________|
    +   G  +   
    5   N  5
    V   D  V  

For more at: http://playground.arduino.cc/Main/LedDisplay
*/

void hcms_291x_init(void);
void hcms_291x_clear(void);
void hcms_291x_home(void);
void hcms_291x_set_cursor(uint8_t whichPosition);
void hcms_291x_write(uint8_t b);
void hcms_291x_scroll(int direction);
void hcms_291x_set_string(char* _displayString);
void hcms_291x_set_brightness(uint8_t bright);
void hcms_291x_write_character(char whatCharacter, uint8_t whatPosition);
void hcms_291x_load_control_register(uint8_t dataByte);
void hcms_291x_load_dot_register(void);
void hcms_291x_shift_out(uint8_t val);
void hcms_291x_print(char* text);

int  hcms_291x_string_length(void);
char* hcms_291x_get_string(void);
uint8_t hcms_291x_get_cursor(void);

#endif
