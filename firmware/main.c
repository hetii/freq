#include "global.h"
#include <util/delay.h>   //delay
#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#include "hcms291x.h"

#define LED D,7
#define INPUT_PIN D,4 //input for counts.
#define CLR D,3 // Reset of 74HCT393E
#define USE_HCMS 1

volatile uint32_t m = 0;
volatile uint32_t s = 0;

/*
long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  
 
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}
*/

//https://gcc.gnu.org/wiki/avr-gcc
ISR (TIMER0_OVF_vect,ISR_NAKED){
    asm(
    "push    r1" "\n\t"
    "push    r0" "\n\t"
    "in      r0, __SREG__" "\n\t"
    "eor     r1, r1" "\n\t"
    "push    r24" "\n\t"
    "push    r25" "\n\t"
    "push    r26" "\n\t"
    "push    r27" "\n\t"
    );
    m++;
    asm(
    "pop     r27" "\n\t"
    "pop     r26" "\n\t"
    "pop     r25" "\n\t"
    "pop     r24" "\n\t"
    "out     __SREG__, r0" "\n\t"
    "pop     r0" "\n\t"
    "pop     r1" "\n\t"
    "reti" "\n\t"
    );
}

//826/(4000000/100) = .02065000000000000000
ISR(TIMER1_COMPA_vect, ISR_NAKED){
    asm(
    "push    r0" "\n"
    "in      r0, __SREG__" "\n\t"
    "push    r1" "\n\t"
    "push    r18" "\n\t"
    "push    r24" "\n\t"
    "push    r25" "\n\t"
    );
    SET(CLR);
    s = (m << 8) + TCNT0;
    //TIFR1=(1<<ICF1)|(1<<TOV1);
    RESET(CLR);
    TCNT0 = 0;
    m = 0;
    asm(
    "pop     r25" "\n\t"
    "pop     r24" "\n\t"
    "pop     r18" "\n\t"
    "pop     r1" "\n\t"
    "out     __SREG__, r0" "\n\t"
    "pop     r0" "\n\t"
    "reti" "\n\t"
    );
}
     
int main(void){ 
    SET_OUTPUT(LED);
    SET_OUTPUT(CLR);
    SET(CLR);
    RESET(CLR);
    USART_init();
    hcms_291x_init();
    hcms_291x_print("Hello");
    uint8_t i = 0;

    for (i=0;i<=10;++i){
        TOGGLE(LED);
        _delay_ms(50);
    }

    uprint("Lets go....\n");     

    OCR1A = 0x3D08; // 16000000 ÷ 1024 - 1 = 15624
    // Set interrupt on compare match.
    TIMSK1 |= (1 << OCIE1A);
    // Mode 4, CTC on OCR1A, set prescaler to 1024 and start the timer.
    TCCR1B |= (1 << WGM12) | (1 << CS12) | (1 << CS10);
    // Ok, time to configure second timer for input.

    // PD4 is now an input with pull-up enabled.
    SET_INPUT(INPUT_PIN);
    //SET(INPUT_PIN);
     
    // Enable timer0 interrupt.
    TIMSK0 |= (1 << TOIE0);

    // External Clock Source on T0 pin, Clock on Rising Edge.
    // TCCR0B |= (1 << CS02) | (1 << CS01) | (1 << CS00);
    // External Clock Source on T0 pin, Clock on Falling Edge.
    TCCR0B |= (1 << CS02) | (1 << CS01) | (0 << CS00);

    sei();

    while (1){
        if(s>0){
//            s = s*16;
#if USE_HCMS == 1
            /*
            i = 7;
            hcms_291x_write_character(s%10+0x30, i);
            for(;((s/=10)||(i!=0));){
                 hcms_291x_write_character(s%10+0x30, --i);
            }
            hcms_291x_load_dot_register();
            */
            hcms_291x_write_character(s/10000000+0x30,0);
            hcms_291x_write_character((s%10000000)/1000000+0x30,1);
            hcms_291x_write_character((s%1000000)/100000+0x30,2);
            hcms_291x_write_character((s%100000)/10000+0x30,3);
            hcms_291x_write_character((s%10000)/1000+0x30,4);
            hcms_291x_write_character((s%1000)/100+0x30,5);
            hcms_291x_write_character((s%100)/10+0x30,6);
            hcms_291x_write_character((s%10)+0x30,7);
            hcms_291x_load_dot_register();
            s = 0;
#else
            USART_send(s/10000000+0x30);
            USART_send((s%10000000)/1000000+0x30);
            USART_send((s%1000000)/100000+0x30);
            USART_send((s%100000)/10000+0x30);
            USART_send((s%10000)/1000+0x30);
            USART_send((s%1000)/100+0x30);
            USART_send((s%100)/10+0x30);
            USART_send((s%10)+0x30);
            USART_send('\n');
            s = 0 ;
#endif
        }
    }
    return 0;
}
