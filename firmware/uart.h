#ifndef _UART_H_
#define _UART_H_

#include <avr/io.h>

#define USART_BAUDRATE 57600
#define BAUD_PRESCALLER (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

void USART_init(void);
void USART_send( unsigned char data);
void uprint(char* text);

#endif
